package com.jee.mdb;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;

import com.jee.ws.ClientProjet;

@MessageDriven(activationConfig = { 
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "Queue02"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, mappedName = "Queue02")
public class MDBReciever implements MessageListener {
	@Inject JMSContext jmsContext;
	@Resource(mappedName="Queue01") Queue queue01;
    public void onMessage(Message message) {
        try {
        	float stringRate = message.getFloatProperty("rate");
			String stringSecretInfo = message.getStringProperty("secretInfo");
			String stringFileTitle = message.getStringProperty("fileTitle");
			String stringKey = message.getStringProperty("key");
			
			System.out.println("Envoie des informations suivantes � l'app C# depuis le MDB: " 
			+ stringRate + ", " + stringSecretInfo + ", " + stringFileTitle + ", " + stringKey);	
			
			new ClientProjet().sendMessageC(stringRate, stringSecretInfo, stringFileTitle, stringKey);
			
		} catch (JMSException e) {
			e.printStackTrace();
		}
    }
}
