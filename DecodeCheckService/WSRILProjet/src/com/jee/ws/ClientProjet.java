package com.jee.ws;

import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jee.ws.ClientProjet;

@WebService(serviceName = "Webservice")
public class ClientProjet {
	
	/*@WebMethod(operationName = "sendText")
	public String getText(String text) {
		return text;
	}*/
	
	@WebMethod(operationName = "sendMessageToJEE")
	public String sendMessageJEE(String message, String fileTitle, String key) throws JMSException {		
		try {
			
			InitialContext context = new InitialContext();
			
			Queue queue01 = (Queue)context.lookup("Queue01");
			
			JMSContext jmsContext = ((ConnectionFactory) context.lookup("ConnectionFactory1")).createContext();
			JMSProducer jmsProducer = jmsContext.createProducer();
			
			Message messageSender = jmsContext.createMessage();
			messageSender.setStringProperty("message", message);
			messageSender.setStringProperty("fileTitle", fileTitle);
			messageSender.setStringProperty("key", key);
			
			jmsProducer.send(queue01, messageSender);
			
			System.out.println("Message: " + message + " envoye dans la Queue01");
			System.out.println("//////////////////////////////////////////////////////////////////");
			
			return message;
			
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@WebMethod(operationName = "sendMessageToC")
	public void sendMessageC(float rate, String stringSecretInfo, String stringFileTitle, String stringKey) {		
		//Send Message To C#
	}
}