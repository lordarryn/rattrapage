
package com.jee.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jee.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendMessageToJEE_QNAME = new QName("http://ws.jee.com/", "sendMessageToJEE");
    private final static QName _SendMessageToCResponse_QNAME = new QName("http://ws.jee.com/", "sendMessageToCResponse");
    private final static QName _SendMessageToC_QNAME = new QName("http://ws.jee.com/", "sendMessageToC");
    private final static QName _SendMessageToJEEResponse_QNAME = new QName("http://ws.jee.com/", "sendMessageToJEEResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jee.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendMessageToC }
     * 
     */
    public SendMessageToC createSendMessageToC() {
        return new SendMessageToC();
    }

    /**
     * Create an instance of {@link SendMessageToJEEResponse }
     * 
     */
    public SendMessageToJEEResponse createSendMessageToJEEResponse() {
        return new SendMessageToJEEResponse();
    }

    /**
     * Create an instance of {@link SendMessageToJEE }
     * 
     */
    public SendMessageToJEE createSendMessageToJEE() {
        return new SendMessageToJEE();
    }

    /**
     * Create an instance of {@link SendMessageToCResponse }
     * 
     */
    public SendMessageToCResponse createSendMessageToCResponse() {
        return new SendMessageToCResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMessageToJEE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jee.com/", name = "sendMessageToJEE")
    public JAXBElement<SendMessageToJEE> createSendMessageToJEE(SendMessageToJEE value) {
        return new JAXBElement<SendMessageToJEE>(_SendMessageToJEE_QNAME, SendMessageToJEE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMessageToCResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jee.com/", name = "sendMessageToCResponse")
    public JAXBElement<SendMessageToCResponse> createSendMessageToCResponse(SendMessageToCResponse value) {
        return new JAXBElement<SendMessageToCResponse>(_SendMessageToCResponse_QNAME, SendMessageToCResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMessageToC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jee.com/", name = "sendMessageToC")
    public JAXBElement<SendMessageToC> createSendMessageToC(SendMessageToC value) {
        return new JAXBElement<SendMessageToC>(_SendMessageToC_QNAME, SendMessageToC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMessageToJEEResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jee.com/", name = "sendMessageToJEEResponse")
    public JAXBElement<SendMessageToJEEResponse> createSendMessageToJEEResponse(SendMessageToJEEResponse value) {
        return new JAXBElement<SendMessageToJEEResponse>(_SendMessageToJEEResponse_QNAME, SendMessageToJEEResponse.class, null, value);
    }

}
