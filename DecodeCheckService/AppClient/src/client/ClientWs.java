package client;

import com.jee.ws.ClientProjet;
import com.jee.ws.Webservice;

public class ClientWs {

	public static void main(String[] args) {
		ClientProjet clientProjet = new Webservice().getClientProjetPort();
				
		System.out.println(clientProjet.sendMessageToJEE("Test WebService"));
	}

}
