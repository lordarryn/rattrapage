package com.jee.message;

import javax.ejb.Remote;

@Remote
public interface MessageManagerRemote {	
	public String[] segMessage(String message);	
}
