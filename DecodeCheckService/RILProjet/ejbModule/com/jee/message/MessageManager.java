package com.jee.message;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class MessageManager
 */
@Stateless(mappedName = "messageManager")
@LocalBean
public class MessageManager implements MessageManagerRemote {    
    public String[] segMessage(String message){    	
    	String[] words = message.replace(".", "").replace(",", "").replace("?", "").replace("!","").replace("'", " ")
    			.replace(":", "").split(" ");
    	return words;
    }
}
