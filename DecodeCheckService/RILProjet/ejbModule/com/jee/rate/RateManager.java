package com.jee.rate;

import java.sql.SQLException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jee.data.DataManagerRemote;
@Stateless(mappedName = "rateManager")
@LocalBean
public class RateManager implements RateManagerRemote {    
    public float calculRate(String[] wordList) throws SQLException {
    	try { 			
 			InitialContext context = new InitialContext();			
 			DataManagerRemote dataManager = (DataManagerRemote) context.lookup("dataManager");
 			
 			int nbWordsTotal = wordList.length;
 	    	int nbWordsCheck = 0;
 	    	float rateWordsCheck = 0;
 	    	
 	    	for(int i = 0; i<wordList.length; i+=1) {
 	    		if (dataManager.checkExistWords(wordList[i])) {
 	    			nbWordsCheck = nbWordsCheck + 1;
 	    			System.out.println("Une correspondance trouvee sur le mot: " + wordList[i]);
 	    		}
 	    	}
 	    	
 	    	rateWordsCheck = nbWordsCheck * 100 / nbWordsTotal;	
 	    	return rateWordsCheck;			
 		} catch (NamingException e) {
 			e.printStackTrace();
 		}
    	
    	return 0;
    }
}
