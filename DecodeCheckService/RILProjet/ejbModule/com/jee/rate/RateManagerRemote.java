package com.jee.rate;

import java.sql.SQLException;

import javax.ejb.Remote;

@Remote
public interface RateManagerRemote {
	public float calculRate(String[] wordList) throws SQLException;
}
