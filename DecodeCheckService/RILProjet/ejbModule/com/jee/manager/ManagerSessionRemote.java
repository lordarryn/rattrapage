package com.jee.manager;

import java.sql.SQLException;

import javax.ejb.Remote;
import javax.jms.JMSException;
import javax.naming.NamingException;

@Remote
public interface ManagerSessionRemote {
	public void ManageDecryptMessage(String message, String fileTitle, String key) 
			throws JMSException, SQLException, NamingException;
}
