package com.jee.manager;

import java.sql.SQLException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Queue;
/*import javax.jms.Message;

import com.jee.mdb.MDBRecieve;
import com.jee.message.MessageManager;
import com.jee.rate.RateManager;*/
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jee.message.MessageManagerRemote;
import com.jee.rate.RateManagerRemote;
import com.jee.sendMail.SendManagerRemote;
import com.sun.enterprise.util.StringUtils;

/**
 * Session Bean implementation class ManagerSession
 */
@Stateless(mappedName = "managerSession")
@LocalBean
public class ManagerSession implements ManagerSessionRemote {

    @SuppressWarnings("unused")
	public void ManageDecryptMessage(String message, String fileTitle, String key) 
    		throws JMSException, SQLException, NamingException {    
    	try {
    		float rateValue = 0;
        	InitialContext context = new InitialContext();
        	
        	System.out.println("//////////////////////////////////////////////////////////////////");
 			System.out.println("ManagerSession a re�u le message suivant: " + message);
 			System.out.println("ManagerSession a re�u le titre fichier suivant: " + fileTitle);
 			System.out.println("ManagerSession a re�u la cle suivante: " + key);
 			System.out.println("//////////////////////////////////////////////////////////////////");
 			
 			MessageManagerRemote messageManager = (MessageManagerRemote) context.lookup("messageManager");
 			RateManagerRemote RateManager = (RateManagerRemote) context.lookup("rateManager");
 			SendManagerRemote SendManager = (SendManagerRemote) context.lookup("sendManager");
 			
 			//Segmentation du message par mot
 			String[] wordsList = messageManager.segMessage(message); 			
 			for(int i = 0; i<wordsList.length; i+=1) {
 				System.out.println("Word " + i + ": " + wordsList[i]);
 	    	}
 			
 			//Calcul du Pourcentage de correspondance avec la BDD Oracle
 			rateValue = RateManager.calculRate(wordsList);
 			System.out.println(rateValue + "% de correspondance avec la BDD Oracle"); 			
 			
 			//Envoie du mail si pourcentage de correspondance > 80%
 			if (rateValue >= 0) {
 				//Recherche de l'information secr�te
 	 			String secretInfo = null;
 	 			
 	 			String startSecretInfo = "information secrete est: ";
 	 			if (message.indexOf(startSecretInfo) != -1) {
 	 				secretInfo = message.split(startSecretInfo)[1].split(".")[0];
 	 				System.out.println("Information secr�te: " + secretInfo);
 	 			}
 	 			
 	 			//Envoie du mail d'avertissement de d�chiffrement trouv�
 	 			//SendManager.sendMail("yoanninquimbert@yahoo.fr", "Test Mail", "Test Body");
 	 			
 				//Envoie de r�ponse au WebService via Queue02 JMS			
 				try {			
 					Queue queue02 = (Queue)context.lookup("Queue02");
 					
 					JMSContext jmsContext = ((ConnectionFactory) context.lookup("ConnectionFactory1")).createContext();
 					JMSProducer jmsProducer = jmsContext.createProducer();
 					
 					Message messageSender = jmsContext.createMessage();
 					messageSender.setFloatProperty("rate", rateValue);
 					messageSender.setStringProperty("fileTitle", fileTitle);
 					messageSender.setStringProperty("key", key);
 					
 					if (secretInfo != null) {
 						messageSender.setStringProperty("secretInfo", secretInfo);
 					}
 					
 					jmsProducer.send(queue02, messageSender); 	
 					
 					System.out.println("Message envoy� dans la Queue02");
 					System.out.println("//////////////////////////////////////////////////////////////////");
 				} catch (NamingException e) {
 					e.printStackTrace();
 				}
 				
 			}
 			
 		} catch (NamingException e) {
 			e.printStackTrace();
 		}
    }
}
