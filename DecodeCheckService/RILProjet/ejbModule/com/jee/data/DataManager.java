package com.jee.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.*;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class DataManager
 */
@Stateless(mappedName = "dataManager")
@LocalBean
public class DataManager implements DataManagerRemote {
    /*public List<DataWordEntity> getWords() throws SQLException, NamingException {    	
    	// Connection BDD
    	InitialContext ctx = new InitialContext();
    	javax.sql.DataSource ds = (javax.sql.DataSource)ctx.lookup("jdbc/Oracle");
    	java.sql.Connection conn = ds.getConnection();
    	
    	System.out.println("connexion...");
    	
    	List<DataWordEntity> words = new ArrayList<DataWordEntity>();
    	
    	try {    		
    		Statement st = conn.createStatement();
    		ResultSet rs = st.executeQuery("select * from words");
    		
    		DataWordEntity word;
    		while (rs.next()) {
    			word = new DataWordEntity();
    			word.setId(rs.getInt(1));
    			word.setDataWord(rs.getString(2));
    			words.add(word);
    		}
    		
		} catch (SQLException ex) {
	         ex.printStackTrace();
	    }
    	
    	conn.close();
    	return words;
    }*/
    
    public Boolean checkExistWords(String word) throws SQLException, NamingException {    	
    	// Connection BDD
    	InitialContext ctx = new InitialContext();
    	javax.sql.DataSource ds = (javax.sql.DataSource)ctx.lookup("jdbc/Oracle");
    	java.sql.Connection conn = ds.getConnection();
    	
    	try {    		
    		Statement st = conn.createStatement();
    		ResultSet result = st.executeQuery("select * from words where word = '" + word + "'");
    		
    		while (result.next()) {
    			result.close();
    			return true;
    		}
		} catch (SQLException ex) {
	         ex.printStackTrace();
	    }
    	
    	conn.close();
    	return false;
    }
}
