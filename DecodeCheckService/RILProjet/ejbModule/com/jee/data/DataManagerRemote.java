package com.jee.data;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Remote;
import javax.naming.NamingException;

@Remote
public interface DataManagerRemote {
	// public List<DataWordEntity> getWords() throws SQLException, NamingException;
	public Boolean checkExistWords(String word) throws SQLException, NamingException;
}
