package com.jee.data;

import java.io.Serializable;

public class DataWordEntity implements Serializable{
	
	private int id;
	private String DataWord;
	
	public DataWordEntity() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDataWord() {
		return DataWord;
	}

	public void setDataWord(String dataWord) {
		DataWord = dataWord;
	}

	
}