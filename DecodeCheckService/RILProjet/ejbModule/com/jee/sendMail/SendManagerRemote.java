package com.jee.sendMail;

import javax.ejb.Remote;

@Remote
public interface SendManagerRemote {
	public void sendMail(String reciverEmailID, String emailSubject, String emailBody);
}
