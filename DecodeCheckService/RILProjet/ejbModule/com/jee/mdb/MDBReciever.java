package com.jee.mdb;

import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.Remote;
import javax.ejb.embeddable.EJBContainer;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.weld.context.ejb.Ejb;

import com.jee.manager.ManagerSession;
import com.jee.manager.ManagerSessionRemote;

@MessageDriven(activationConfig = { 
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "Queue01"), 
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, mappedName = "Queue01")
public class MDBReciever implements MessageListener {
	@Inject JMSContext jmsContext;
	@Resource(mappedName="Queue02") Queue queue02;
    public void onMessage(Message message) {
        try {
			String stringMessage = message.getStringProperty("message");
			String stringFileTitle = message.getStringProperty("fileTitle");
			String stringKey = message.getStringProperty("key");
			
			System.out.println("MDBRecieve a re�u le message suivant: " + stringMessage);			
			System.out.print(stringMessage + ", " + stringFileTitle + ", " + stringKey);
			
			InitialContext context = new InitialContext();			
			ManagerSessionRemote managerSession = (ManagerSessionRemote) context.lookup("managerSession");
			
			managerSession.ManageDecryptMessage(stringMessage, stringFileTitle, stringKey);			
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    /*public void sendMessageDecryption(Message message) {
        try {
			String stringMessage = message.getBody(String.class);
			System.out.println("Informations renvoy� � l'application c#: " + stringMessage);
			jmsContext.createProducer().send(queue02, message);
		} catch (JMSException e) {
			e.printStackTrace();
		}
    }*/
}