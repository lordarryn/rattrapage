package client;

import com.jee.ws.ClientProjet;
import com.jee.ws.JMSException_Exception;
import com.jee.ws.Webservice;

public class ClientWs2Test {

	public static void main(String[] args) throws JMSException_Exception {
		ClientProjet clientProjet = new Webservice().getClientProjetPort();
				
		System.out.println(clientProjet.sendMessageToJEE("L'information secrete est: Test WebService. acclamations",
				"File01", "Test"));
	}
}
