
package com.jee.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "JMSException", targetNamespace = "http://ws.jee.com/")
public class JMSException_Exception
    extends java.lang.Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private JMSException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public JMSException_Exception(String message, JMSException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public JMSException_Exception(String message, JMSException faultInfo, java.lang.Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: com.jee.ws.JMSException
     */
    public JMSException getFaultInfo() {
        return faultInfo;
    }

}
