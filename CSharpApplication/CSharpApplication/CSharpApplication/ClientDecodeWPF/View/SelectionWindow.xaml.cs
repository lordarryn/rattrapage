﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClientDecodeWPF.Model;
using ClientDecodeWPF.ViewModel;

namespace ClientDecodeWPF.View
{
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class SelectionWindow : Window
    {
        DocumentViewModel documentViewModel;
        public SelectionWindow()
        {
            InitializeComponent();
            documentViewModel = new DocumentViewModel();
            DataContext = documentViewModel;

            LB1.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription( "Id" , System.ComponentModel.ListSortDirection.Ascending));
        }

        public void Button_Click(object sender, EventArgs arg)
        {
            List<Document> mySelectedItems = new List<Document>();

            foreach (Document doc in LB1.SelectedItems)
            {
                mySelectedItems.Add(doc);
            }

            documentViewModel.SetSelectedDocs(mySelectedItems);
        }
    }
}
