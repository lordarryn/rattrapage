﻿using ClientDecodeWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ClientDecodeWPF.Command
{
    class SignUpCommand : ICommand
    {
        private UserViewModel userVM;
        public SignUpCommand(UserViewModel userViewModel)
        {
            userVM = userViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return String.IsNullOrWhiteSpace(userVM.User.Error);
        }

        public void Execute(object parameter)
        {
            userVM.Signup(parameter);
        }
    }
}

