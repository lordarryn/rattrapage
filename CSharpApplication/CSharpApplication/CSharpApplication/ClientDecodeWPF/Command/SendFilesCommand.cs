﻿using ClientDecodeWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ClientDecodeWPF.Command
{
    class SendFilesCommand : ICommand
    {
        private DocumentViewModel documentVM;
        public SendFilesCommand(DocumentViewModel documentViewModel)
        {
            documentVM = documentViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return documentVM.CanSendFile;
        }

        public void Execute(object parameter)
        {
            documentVM.SendFiles();
        }
    }
}
