﻿using ClientDecodeWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ClientDecodeWPF.Command
{
    class LoginCommand : ICommand
    {
        private UserViewModel userVM;
        public LoginCommand(UserViewModel userViewModel)
        {
            this.userVM = userViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return String.IsNullOrWhiteSpace(userVM.User.Error);
        }

        public void Execute(object parameter)
        {
            userVM.Login(parameter);
        }
    }
}
