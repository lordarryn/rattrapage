﻿using ClientDecodeWPF.Command;
using ClientDecodeWPF.Model;
using ClientDecodeWPF.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace ClientDecodeWPF.ViewModel
{
    class UserViewModel
    {
        private User user;
        private string key;
        public UserViewModel()
        {
            user = new User(0, "", "", "", "");
            LoginCommand = new LoginCommand(this);
            SignUpCommand = new SignUpCommand(this);
            key = "1011001010";
        }

        public User User { get => user;}

        public ICommand LoginCommand
        {
            get;
            private set;
        }

        public ICommand SignUpCommand
        {
            get;
            private set;
        }

        internal void Login(object obj)
        {
            PasswordBox pwBox = obj as PasswordBox;
            Window window;

            SDES sdes = new SDES();

            var encrypted = sdes.Encrypt(pwBox.Password, key);
            //Trace.WriteLine(encrypted);
            //var decrypted = sdes.Decrypt(encrypted, key);
            //Trace.WriteLine(decrypted);

            ApiOperations ops = new ApiOperations();

            User user = new User(1, "test", "password", "email@email.com", "test");

            /* pas de connexion BDD pour test */

            //User user = ops.AuthenticateUser(User.Name, encrypted);

            if (user == null)
            {
                MessageBox.Show("Invalid username or password");
                window = new LoginWindow();
                window.Show();
                return;
            }

            Globals.LoggedInUser = user;
            MessageBox.Show("Login successful");

            window = new SelectionWindow();
            window.Show();

        }

        internal void Signup(object obj)
        {
            PasswordBox pwBox = obj as PasswordBox;
            Window window;

            SDES sdes = new SDES();

            var encrypted = sdes.Encrypt(pwBox.Password, key);
            ApiOperations ops = new ApiOperations();

            User user = ops.RegisterUser(User.Name, encrypted, User.Email);
            if (user == null)
            {
                MessageBox.Show("Email already exists");
                window = new LoginWindow();
                window.Show();
                return;
            }

            Globals.LoggedInUser = user;
            MessageBox.Show("Registration successful");

            window = new SelectionWindow();
            window.Show();
        }
    }
}
