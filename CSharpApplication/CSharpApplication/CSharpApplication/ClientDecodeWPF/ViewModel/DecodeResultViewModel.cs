﻿using ClientDecodeWPF.Model;
using ClientDecodeWPF.MiddlewareReference;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.ServiceModel;

namespace ClientDecodeWPF.ViewModel
{
    [CallbackBehavior(UseSynchronizationContext = false)]
    class DecodeResultViewModel : IWCFServiceCallback
    {
        private ObservableCollection<Model.DecodeResult> decodeResults;
        private ParallelOptions opt = new ParallelOptions();
        public DecodeResultViewModel()
        {
            decodeResults = new ObservableCollection<Model.DecodeResult>();
            //decodeResults.Add( new Model.DecodeResult("DocName", "DecodeKey", "This is the secret info"));
            //decodeResults.Add(new Model.DecodeResult("DocName2", "DecodeKey2", "This is another secret info"));
        }

        public void AddDecodedFile(string docname, string key, string info)
        {
            Debug.WriteLine("Callback called: " + docname + " " + key + " " + info);
            
            //decodeResults.Add(new Model.DecodeResult(docname, key, info));
        }

        public void DecodeWCFAsync(ObservableCollection<Document> documents)
        {
            opt.MaxDegreeOfParallelism = Environment.ProcessorCount; //maximum of cores
            var locker = new object();
            int count = 0;

            
            Parallel.ForEach<Document>(documents, opt, i =>
            {
                Interlocked.Increment(ref count);
                Trace.WriteLine("Number of active threads:" + count);

                InstanceContext context = new InstanceContext(this);
                WCFServiceClient service = new WCFServiceClient(context);
                //File file = new File { Id = i.Id, Lines = i.Lines.ToArray(), Name = i.Name };
                service.f_service(i.Id, i.Lines.ToArray(), i.Name);
                Interlocked.Decrement(ref count);
            });
            //Parallel.For
            //(0
            // , 1000
            // , new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }
            // , (i) =>
            // {
            //     Interlocked.Increment(ref count);
            //     lock (locker)
            //     {
            //         Console.WriteLine("Number of active threads:" + count);
            //         Thread.Sleep(10);
            //     }
            //     Interlocked.Decrement(ref count);
            // }
            //);
        }


        public ObservableCollection<Model.DecodeResult> DecodeResult { get => decodeResults; }
    }
}
