﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;
using ClientDecodeWPF.Model;
using System.Net;

namespace ClientDecodeWPF.ViewModel
{
    class ApiOperations
    {
        /**
         * Base Url @string
         */
        private string baseUrl;
        private string appToken = "";

        public ApiOperations()
        {
            this.baseUrl = "http://localhost:52913/api";
        }

        public User AuthenticateUser(string username, string password)
        {
            string endpoint = this.baseUrl + "/Users/login";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                username = username,
                password = password
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            try
            {
                string response = wc.UploadString(endpoint, method, json);
                return JsonConvert.DeserializeObject<User>(response);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public User GetUserDetails(User user)
        {
            string endpoint = this.baseUrl + "/Users/" + user.Id;
            string userToken = user.Token;

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            wc.Headers["Authorization"] = userToken;
            try
            {
                string response = wc.DownloadString(endpoint);
                user = JsonConvert.DeserializeObject<User>(response);
                user.Token = userToken;
                return user;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public User RegisterUser(string username, string password, string email)
        {
            string endpoint = this.baseUrl + "/Users";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                username = username,
                password = password,
                email = email,
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            wc.Headers["Authorization"] = appToken;
            try
            {
                string response = wc.UploadString(endpoint, method, json);
                return JsonConvert.DeserializeObject<User>(response);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool ValidateToken(string token)
        {
            string endpoint = this.baseUrl + "/Users/ValidateToken";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                token = token,
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            try
            {
                string response = wc.UploadString(endpoint, method, json);
                return JsonConvert.DeserializeObject<bool>(response);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
