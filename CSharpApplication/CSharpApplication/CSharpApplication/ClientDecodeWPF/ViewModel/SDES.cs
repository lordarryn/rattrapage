﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientDecodeWPF.ViewModel
{
    class SDES
    {
        private Sandbox S0;
        private Sandbox S1;

        public SDES()
        {
            this.S0 = new Sandbox(new[,]
            {
                { new [] { false, true }, new [] { false, false }, new [] { true, true }, new [] { true, false } },
                { new [] { true, true }, new [] { true, false }, new [] { false, true }, new [] { false, false } },
                { new [] { false, false }, new [] { true, false }, new [] { false, true }, new [] { true, true } },
                { new [] { true, true }, new [] { false, true }, new [] { true, true }, new [] { true, false } }
            });
            this.S1 = new Sandbox(new[,]
            {
                { new [] { false, false }, new [] { false, true }, new [] { true, false }, new [] { true, true } },
                { new [] { true, false }, new [] { false, false }, new [] { false, true }, new [] { true, true } },
                { new [] { true, true }, new [] { false, false }, new [] { false, true }, new [] { false, false } },
                { new [] { true, false }, new [] { false, true }, new [] { false, false }, new [] { true, true } }
            });
        }

        public string Encrypt(string sentence, string masterKey)
        {
            var key = masterKey.Take(10).Select(x => x != '0').ToArray();
            var result = sentence.Select(c => this.EncryptChar(c, key)).ToList();
            return string.Concat(result);
        }

        public string Decrypt(string sentence, string masterKey)
        {
            var key = masterKey.Take(10).Select(x => x != '0').ToArray();
            var result = sentence.Select(c => this.DecryptChar(c, key)).ToList();
            return string.Concat(result);
        }

        private char EncryptChar(char character, bool[] key)
        {
            var keys = this.GenerateKeys(key);
            var k1 = keys[0];
            var k2 = keys[1];
            var bitArray = new System.Collections.BitArray(System.Text.Encoding.Default.GetBytes(new[] { character }));
            bool[] boolArray = new bool[bitArray.Length];
            for (int i = 0; i < bitArray.Length; i++)
                boolArray[i] = bitArray[i];
            var ip = Ip(boolArray);
            var fk = this.Fk(ip, k1);
            fk = this.sw(fk);
            fk = this.Fk(fk, k2);
            var rip = Rip(fk);
            var _bitArray = new System.Collections.BitArray(rip);
            byte[] bytes = new byte[_bitArray.Length];
            _bitArray.CopyTo(bytes, 0);
            var result = System.Text.Encoding.Default.GetString(bytes).ToCharArray().First();

            return result;
        }

        private char DecryptChar(char character, bool[] key)
        {
            var keys = this.GenerateKeys(key);
            var k1 = keys[0];
            var k2 = keys[1];
            var bitArray = new System.Collections.BitArray(System.Text.Encoding.Default.GetBytes(new[] { character }));
            bool[] boolArray = new bool[bitArray.Length];
            for (int i = 0; i < bitArray.Length; i++)
                boolArray[i] = bitArray[i];
            var ip = Ip(boolArray);
            var fk = this.Fk(ip, k2);
            fk = this.sw(fk);
            fk = this.Fk(fk, k1);
            var rip = Rip(fk);

            var _bitArray = new System.Collections.BitArray(rip);
            byte[] bytes = new byte[_bitArray.Length];
            _bitArray.CopyTo(bytes, 0);
            var result = System.Text.Encoding.Default.GetString(bytes).ToCharArray().First();

            return result;
        }

        private List<bool[]> GenerateKeys(bool[] key)
        {
            var masterKey = P10(key);
            var leftKey = masterKey.Take(masterKey.Length / 2).ToArray();
            var rightKey = masterKey.Skip(masterKey.Length / 2).ToArray();

            leftKey = CircularLeftShift(leftKey, 1);
            rightKey = CircularLeftShift(rightKey, 1);

            bool[] k1 = P8(MergeArrays(leftKey, rightKey));

            leftKey = CircularLeftShift(leftKey, 2);
            rightKey = CircularLeftShift(rightKey, 2);

            bool[] k2 = P8(MergeArrays(leftKey, rightKey));

            return new List<bool[]> { k1, k2 };
        }

        internal static bool[] P10(bool[] toPermute)
        {
            var p = new List<int> { 3, 5, 2, 7, 4, 10, 1, 9, 8, 6 };
            return Permute(toPermute, p);
        }

        internal static bool[] P8(bool[] toPermute)
        {
            var p = new List<int> { 6, 3, 7, 4, 8, 5, 10, 9 };
            return Permute(toPermute, p);
        }

        internal static bool[] Ep(bool[] toPermute)
        {
            var p = new List<int> { 4, 1, 2, 3, 2, 3, 4, 1 };
            return Permute(toPermute, p);
        }

        internal static bool[] Ip(bool[] toPermute)
        {
            var p = new List<int> { 2, 6, 3, 1, 4, 8, 5, 7 };
            return Permute(toPermute, p);
        }

        internal static bool[] Rip(bool[] toPermute)
        {
            var p = new List<int> { 4, 1, 3, 5, 7, 2, 8, 6 };
            return Permute(toPermute, p);
        }

        internal static bool[] P4(bool[] toPermute)
        {
            var p = new List<int> { 2, 4, 3, 1 };
            return Permute(toPermute, p);
        }

        private static bool[] Permute(bool[] toPermute, List<int> p)
        {
            var permuted = new bool[p.Count];
            for (var i = 0; i < p.Count; i++)
                permuted[i] = toPermute[p[i] - 1];
            return permuted;
        }


        public static bool[] CircularLeftShift(bool[] array, int offset)
        {
            var shiftedArray = new bool[array.Length];
            Buffer.BlockCopy(array, offset * sizeof(bool), shiftedArray, 0, (array.Length - offset) * sizeof(bool));
            Buffer.BlockCopy(array, 0, shiftedArray, (array.Length - offset) * sizeof(bool), offset * sizeof(bool));
            return shiftedArray;
        }

        private bool[] F(bool[] input, bool[] subkey)
        {
            var key = xor(Ep(input), subkey);
            var left = key.Take(key.Length / 2).ToArray();
            var right = key.Skip(key.Length / 2).ToArray();
            var leftResult = this.S0.GetMatrixVal(new[] { left[0], left[3] }, new[] { left[1], left[2] });
            var rightResult = this.S1.GetMatrixVal(new[] { right[0], right[3] }, new[] { right[1], right[2] });
            var result = MergeArrays(leftResult, rightResult);

            return P4(result);
        }

        private bool[] Fk(bool[] bits, bool[] key)
        {
            var left = bits.Take(bits.Length / 2).ToArray();
            var right = bits.Skip(bits.Length / 2).ToArray();
            var result = xor(left, this.F(right, key));
            return MergeArrays(result, right);
        }

        private bool[] sw(bool[] input)
        {
            var left = input.Take(input.Length / 2).ToArray();
            var right = input.Skip(input.Length / 2).ToArray();
            return MergeArrays(right, left);
        }

        public static bool[] xor(bool[] left, bool[] right)
        {
            var results = new bool[left.Length];
            System.Threading.Tasks.Parallel.For(0, left.Length, i => {
                results[i] = left[i] ^ right[i];
            });
            return results;
        }

        public static T[] MergeArrays<T>(T[] left, T[] right)
        {
            var merged = new T[left.Length + right.Length];
            left.CopyTo(merged, 0);
            right.CopyTo(merged, left.Length);
            return merged;
        }

        class Sandbox
        {
            private readonly bool[,][] _matrix;

            public Sandbox(bool[,][] matrix)
            {
                this._matrix = matrix;
            }

            public bool[] GetMatrixVal(bool[] left, bool[] right)
            {
                var x = this.GetRange(left);
                var y = this.GetRange(right);
                return this._matrix[x, y];
            }

            private int GetRange(bool[] booleans)
            {
                var left = booleans[0];
                var right = booleans[1];
                if (left == false && right == false) return 0;
                if (left == false) return 1;
                if (right == false) return 2;
                return 3;
            }
        }
    }
}
