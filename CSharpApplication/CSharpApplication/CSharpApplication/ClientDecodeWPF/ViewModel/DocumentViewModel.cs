﻿using ClientDecodeWPF.Command;
using ClientDecodeWPF.Model;
using ClientDecodeWPF.View;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClientDecodeWPF.ViewModel
{
    class DocumentViewModel : INotifyPropertyChanged
    {
        private DecodeResultViewModel decodeResultVM;
        private ObservableCollection<Document> documents;
        private ParallelOptions opt = new ParallelOptions();
        private UserViewModel userViewModel;
        private ObservableCollection<Document> selectedDocs;
        public DocumentViewModel()
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.DataContext = userViewModel;
            if (checkIfValidUser())
            {
                Trace.WriteLine("valid");
            } else
            {
                Trace.WriteLine("invalid");
            }
            decodeResultVM = new DecodeResultViewModel();
            documents = new ObservableCollection<Document>();
            selectedDocs = new ObservableCollection<Document>();
            SendFileCommand = new SendFilesCommand(this);
            Thread ReadThread = new Thread(new ThreadStart(ReadDocs));
            ReadThread.Start();

        }


        public void ReadDocs()
        {
            IEnumerable<String> filePaths = GetAllFiles("C:\\Users\\Mathieu\\source\\repos\\rattrapage\\CSharpApplication\\CSharpApplication\\CSharpApplication\\ClientDecodeWPF\\Files", "*.txt");
           
            opt.MaxDegreeOfParallelism = Environment.ProcessorCount; 

            //Task readTask = Task.Factory.StartNew(() =>
            //{
            try
                {
                    List<string> _filePaths = filePaths.ToList();
                    Parallel.ForEach<string>(_filePaths, opt, filePath => {

                        string filename = Regex.Replace(Path.GetFileName(filePath), @"(\.txt)", "");

                        int id = int.Parse(filename.Substring(filename.Length - 3));

                        List<string> lines = new List<string>();
                        
                        using (FileStream fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        using (BufferedStream bs = new BufferedStream(fs))
                        using (StreamReader sr = new StreamReader(bs))
                        {
                            string line;
                            while ((line = sr.ReadLine()) != null)
                            {
                                lines.Add(line);
                            }
                            fs.Close();
                        }

                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            documents.Add(new Document(id, filename, lines));
                        });
                        OnPropertyChanged("Documents");
                    });
                }
                catch (Exception e)
                {
                Debug.Assert(false, String.Format("While reading the files" + Environment.NewLine + e.Message.ToString()));
            }
            //});
        }

        /// <summary>This methode gets all the file paths from a directory </summary>
        /// <param name="path">path of the directory to get files from</param>
        /// <param name="searchPattern">get specific files. If you want only exe files you enter *.exe</param>
        private static IEnumerable<string> GetAllFiles(string path, string searchPattern)
        {
            return Directory.EnumerateFiles(path, searchPattern).Union(
            Directory.EnumerateDirectories(path).SelectMany(d =>
            {
                try
                {
                    return GetAllFiles(d, searchPattern);
                }
                catch (Exception e)
                {
                    return Enumerable.Empty<string>();
                }
            }));
        }

        internal void SendFiles()
        {
            decodeResultVM.DecodeWCFAsync(selectedDocs);
            //Trace.WriteLine(selectedDocs);
        }

        public ObservableCollection<Document> Documents { 
            get {
                return documents;
            }
        }

        internal void SetSelectedDocs(List<Document> mySelectedItems)
        {
            if( mySelectedItems != null)
            {
                SelectedDocs.Clear();
                foreach (Document doc in mySelectedItems)
                {
                    SelectedDocs.Add(doc);
                }
            }
        }

        public ObservableCollection<Document> SelectedDocs
        {
            get { return selectedDocs; }
            set
            {
                selectedDocs = value;
                OnPropertyChanged("SelectedDoc");
            }
        }

        public ICommand SendFileCommand
        {
            get;
            private set;
        }

        public bool CanSendFile
        {
            get
            {
                if (documents == null) return false;
                //return documents.Any();
                if (selectedDocs == null) return false;
                return selectedDocs.Any();
            }
        }

        public DecodeResultViewModel DecodeResultVM { get => decodeResultVM; }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool checkIfValidUser()
        {
            User user = Globals.LoggedInUser;
            if (user == null)
            {
                Trace.WriteLine("no user");
                return false;
            }
            else
            {
                ApiOperations ops = new ApiOperations();
                return ops.ValidateToken(user.Token);
            }

        }
    }
}
