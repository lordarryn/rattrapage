﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ClientDecodeWPF.Model
{
    class Document : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private List<string> lines;
        public Document(int documentId, string documentName, List<string> documentLines)
        {
            Id = documentId;
            Name = documentName;
            Lines = documentLines;
        }


        public int Id { 
            get => id; 
            set { 
                id = value;
                OnPropertyChanged("Id");
            } 
        }
        public string Name
        {
            get => name; 
            set {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public List<string> Lines { 
            get => lines;
            set {
                lines = value;
                OnPropertyChanged("Lines");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
