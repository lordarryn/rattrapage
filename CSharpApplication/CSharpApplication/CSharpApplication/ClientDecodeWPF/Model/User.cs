﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ClientDecodeWPF.Model
{
    class User : INotifyPropertyChanged, IDataErrorInfo
    {
        private int id;
        private string name;
        private string email;
        private string password;
        private string token;

        public User(int userId, string userName, string userEmail, string userPassword, string userToken)
        {
            Id = userId;
            Name = userName;
            Email = userEmail;
            Password = userPassword;
            Token = userToken;
        }

        public int Id { 
            get => id; 
            set {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        public string Name {
            get => name;
            set {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Email {
            get => email;
            set {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        public string Password { 
            get => password;
            set {
                password = value;
                OnPropertyChanged("Password");
            }
        }

        public string Token
        {
            get => token;
            set
            {
                token = value;
                OnPropertyChanged("Token");
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public string this[string columnName]
        {
            get
            {
                if (columnName == "Name")
                {
                    if (String.IsNullOrWhiteSpace(Name))
                    {
                        Error = "Name cannot be null or empty";
                    }
                    else
                    {
                        Error = null;
                    }
                }
                else if (columnName == "Email")
                {
                    if (String.IsNullOrWhiteSpace(Email))
                    {
                        Error = "Email cannot be null or empty";
                    }
                    else
                    {
                        Error = null;
                    }
                }
                else if (columnName == "Password")
                {
                    if (String.IsNullOrWhiteSpace(Password))
                    {
                        Error = "Password cannot be null or empty";
                    }
                    else
                    {
                        Error = null;
                    }
                }
                return Error;
            }
        }
        public string Error
        {
            get;
            private set;
        }
    }
}
