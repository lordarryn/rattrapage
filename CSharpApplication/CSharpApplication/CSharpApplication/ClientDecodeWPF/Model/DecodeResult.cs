﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ClientDecodeWPF.Model
{
    class DecodeResult : INotifyPropertyChanged
    {
        private string documentName;
        private string key;
        private string secretInfo;

        public DecodeResult(string decodedDocumentName, string decodeKey, string decodeSecretInfo)
        {
            DocumentName = decodedDocumentName;
            Key = decodeKey;
            SecretInfo = decodeSecretInfo;
        }

        public string DocumentName { 
            get => documentName;
            set { 
                documentName = value;
                OnPropertyChanged("DocumentName");
            }
        }

        public string Key { 
            get => key;
            set { 
                key = value;
                OnPropertyChanged("Key");
            }
        }

        public string SecretInfo { 
            get => secretInfo;
            set { 
                secretInfo = value;
                OnPropertyChanged("SecretInfo");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
