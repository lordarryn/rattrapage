﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackEndAPI.Models;
using System.Net.Http;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BackEndAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserContext _context;
        private IConfiguration _config;

        public UsersController(UserContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            string token = this.Request.Headers["Authorization"];
            if (token == null)
                return BadRequest(new { error = "No authorization header" });

            //bool isValid = JWTAuth.ValidateToken(token, id);
            //if (!isValid)
            //{
            //    this.Response.StatusCode = 403;
            //    return new ObjectResult(new { error = "Invalid access token" });
            //}

            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // GET: api/Users/byEmail?email=user@email.com
        [HttpGet("byEmail")]
        public async Task<ActionResult<User>> GetUserByEmail(string email)
        {
            //var user = await (from _user in _context.Users
            //                  where _user.Email == email
            //                  select _user).FirstAsync();

            var user = await _context.Users
                .Where(_user => _user.Email == email)
                .FirstAsync();

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // POST: api/Users/login
        [HttpPost("login")]
        public async Task<ActionResult<User>> Login([FromBody] User user)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User _user = await _context.Users
                .Where(_user => _user.Username == user.Username && _user.Password == user.Password)
                .FirstAsync();


            if (_user == null)
                return new UnauthorizedResult();

            string accessToken = GenerateJSONWebToken(_user);
            return new ObjectResult(new
            {
                _user.Id,
                _user.Username,
                Token = accessToken
            });
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_context.Users.FirstOrDefault(u => u.Email == user.Email) != null)
                return BadRequest(new { error = "Email already in use" });

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            string accessToken = GenerateJSONWebToken(user);
            
            return CreatedAtAction("GetUser", new { id = user.Id }, new ObjectResult(new
            {
                user.Id,
                user.Username,
                access_token = accessToken
            }));
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return user;
        }

        // POST: api/Users/ValidateToken
        [HttpPost("ValidateToken")]
        public ActionResult<bool> ValidateToken(string token)
        {
            var validationParameters = new TokenValidationParameters()
            {
                ValidIssuer = _config["Jwt:Issuer"],
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken = null;
            try
            {
                tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
            }
            catch (SecurityTokenException)
            {
                return false;
            }
            catch (Exception e)
            {
                //something else happened
                return BadRequest(new { error = "somthing happend : " + e }); ;
            }
            //... manual validations return false if anything untoward is discovered
            return validatedToken != null;
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        public string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
