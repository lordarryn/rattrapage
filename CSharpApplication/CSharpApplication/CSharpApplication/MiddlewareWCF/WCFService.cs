﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFInterfaces;

namespace MiddlewareWCF
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class WCFService : IWCFService
    {

        public void f_service(int id, string[] lines, string name)
        {

            Task.Run(() =>
            {

                Debug.WriteLine("Fichier reçu...");

                Decrypt d = new Decrypt();

                // Generate key
                string key = d.GenerateKey();

                // While generating key keep continue
                while (key != "")
                {


                    // output
                    List<string> outputs = new List<string>();

                    // Loop through all lines
                    foreach (string line in lines)
                    {
                        // push the xor to the final array
                        outputs.Add(d.Xor(line, key));
                    }

                    // Generate a new key
                    key = d.GenerateKey(key);
                }

                OperationContext.Current.GetCallbackChannel<IWCFCallback>().AddDecodedFile("test", "test", "test");
            });
        }
    }
}
