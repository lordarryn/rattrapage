﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFInterfaces
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IService1" à la fois dans le code et le fichier de configuration.
    [ServiceContract(CallbackContract = typeof(IWCFCallback))]
    public interface IWCFService
    {
        [OperationContract]
        void f_service(int id, string[] lines, string name);

        //[OperationContract]
        //DecodeResult m_service(MSG msg);
    }

    public interface IWCFCallback
    {
        [OperationContract]
        void AddDecodedFile(string docname, string key, string info);
    }

    [DataContract]
    public class DecodeResult
    {
        string documentName;
        string key;
        string secretInfo;

        [DataMember]
        public string DocumentName
        {
            get { return documentName; }
            set { documentName = value; }
        }

        [DataMember]
        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        [DataMember]
        public string SecretInfo
        {
            get { return secretInfo; }
            set { secretInfo = value; }
        }
    }

    [DataContract]
    public class File
    {
        private int id;
        private string name;
        private List<string> lines;

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public List<string> Lines
        {
            get { return lines; }
            set { lines = value; }
        }
    }

    [DataContract]
    public struct MSG
    {
        private bool statutOp;
        private string info;
        private object[] data;
        private string operationName;
        private string tokenApp;
        private string tokenUser;
        private string appVersion;
        private string operationVersion;

        [DataMember]
        public bool StatutOp
        {
            get { return statutOp; }
            set { statutOp = value; }
        }

        [DataMember]
        public string Info
        {
            get { return info; }
            set { info = value; }
        }

        [DataMember]
        public object[] Data
        {
            get { return data; }
            set { data = value; }
        }

        [DataMember]
        public string OperationName
        {
            get { return operationName; }
            set { operationName = value; }
        }

        [DataMember]
        public string TokenApp
        {
            get { return tokenApp; }
            set { tokenApp = value; }
        }

        [DataMember]
        public string TokenUser
        {
            get { return tokenUser; }
            set { tokenUser = value; }
        }

        [DataMember]
        public string AppVersion
        {
            get { return appVersion; }
            set { appVersion = value; }
        }

        public string OperationVersion
        {
            get { return operationVersion; }
            set { operationVersion = value; }
        }
    }
}
